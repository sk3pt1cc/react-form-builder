import React from 'react';
import FormBuilder from './components/FormBuilder';
import formtypes from './formtypes';

const App = () => {
  const [someVal, setSomeVal] = React.useState('Give yourself a try.');
  const json = {
    name: {
      type: formtypes.INPUT,
      value: someVal,
      setter: setSomeVal,
    },
  };

  return <FormBuilder json={json} />;
};

export default App;
