import React from 'react';
import PropTypes from 'prop-types';

const Input = (props) => {
  const {
    className,
    styled,
    value,
    setter,
    placeholderText,
  } = props;

  console.log(props);

  if (styled) {
    console.log('This is a styled component.');
  }

  return (
    <input
      placeholder={placeholderText}
      className={className}
      value={value}
      onChange={(e) => setter(e.target.value)}
    />
  );
};

Input.defaultProps = {
  className: Math.random().toString(),
  styled: null,
  placeholderText: '',
};

Input.propTypes = {
  className: PropTypes.string,
  styled: PropTypes.shape(),
  placeholderText: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
  ]).isRequired,
  setter: PropTypes.func.isRequired,
};

export default Input;
