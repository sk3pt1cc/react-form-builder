/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import Input from '../../elements/Input';
import formtypes from '../../../formtypes';

// DO I USE CONTEXT API TO FEED PROPS TO ELEMENTS INSTEAD? HMMMMM. Might be better given the number of them?

const ElementPicker = ({ form }) => Object.entries(form).map(([k, element]) => {
  switch (element.type) {
    case formtypes.INPUT:
      return (
        <Input {...element} key={k} />
      );
    default:
      return <span>Unrecognised element.</span>;
  }
});

export default ElementPicker;
