import React from 'react';
import PropTypes from 'prop-types';
import ElementPicker from './ElementPicker';

export const FormContext = React.createContext({});

const FormBuilder = ({ json }) => (
  <FormContext.Provider value={json}>
    <form>
      <ElementPicker form={json} />
    </form>
  </FormContext.Provider>
);

FormBuilder.propTypes = {
  json: PropTypes.shape().isRequired,
};

export default FormBuilder;
