# Form Builder

_Form JSON_

All types available as imports. Import { formtypes } to access them. This is the best way as it ensures correct spelling of typenames.

If you provide a `styled` block then the component will make use of `styled-components` library.

`anyOtherProps` can be events like `onFocus`.

Elements will appear ordered as they are in the json object.

```
{
    nameInput: {
        className: 'xxx',
        type: 'input',
        value: someValueVariable,
        setter: someSetterFn,
        styled: {
            border: '1px solid black'
        },
        placeholderText: 'Some placeholder text',
        ...anyOtherProps
    }
}
```